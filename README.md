# Twitter users from Chile

## Dependencies

* Python 3
    * Numpy
    * [python-twitter](https://python-twitter.readthedocs.io/en/latest/index.html)
    * Jupyter Notebook or Jupyter Lab

## Setup

### Twitter API keys

You must create two JSON files, and fill them with your Twitter API keys and secrets:

* api_keys/access.json

```json
{
    "token": "",
    "token_secret": ""
}
```

* api_keys/consumer.json

```json
{
    "key": "",
    "secret": ""
}
```

## Running the code

### Download data

```sh
./download_data.sh
```


