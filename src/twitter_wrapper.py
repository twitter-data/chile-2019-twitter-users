import json
import twitter
from datetime import datetime

def get_api(consumer_filename, access_filename):
    consumer_file = open(consumer_filename, "r")
    consumer = json.load(consumer_file)
    consumer_file.close()

    access_file = open(access_filename, "r")
    access = json.load(access_file)
    access_file.close()

    api = twitter.Api(consumer_key=consumer["key"],
                      consumer_secret=consumer["secret"],
                      access_token_key=access["token"],
                      access_token_secret=access["token_secret"])

    return api

def add_user(users, res_user):
    id = res_user.id_str
    if id in users:
        users[id]['num_of_tweets'] += 1
    else:
        user = {
            id: id,
            'created_at': res_user.created_at,
            'screen_name': res_user.screen_name,
            'num_of_tweets': 1,
            'description': res_user.description
        }
        users[id] = user

def get_column_names():
    return ['id', 'screen_name', 'created_at', 'num_of_tweets', 'description']

def get_users_from_search(api, hashtag, users_ids, num_of_tweets):
    users = {}
    users_list = []
    results = api.GetSearch(raw_query="q=%23" + hashtag + "&count=" + str(num_of_tweets))

    columns = get_column_names()

    for result in results:
        add_user(users, result.user)

        if not result.retweeted_status is None:
            add_user(users, result.retweeted_status.user)

    for id, user in users.items():
        if id in users_ids:
            continue

        dt = datetime.strptime(user['created_at'], '%a %b %d %X %z %Y')
        description = " ".join(user['description'].split()).replace(';', ',')
        users_list.append([id, user['screen_name'], dt, user['num_of_tweets'], description])

    return users_list, columns

