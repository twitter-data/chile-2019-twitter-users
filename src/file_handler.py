import numpy as np

def read_user_ids(f):
    user_ids = []
    for line in f:
        user_list = line.split(';')

        if user_list[0] == 'id':
            continue

        user_ids.append(user_list[0])
    
    return user_ids

def read_users(f):
    users = {}
    for line in f:
        user_list = line.split(';')

        if user_list[0] == 'id':
            continue

        user = {
            id: user_list[0]
        }

        users[user['id']] = user
    return users

def save_users(f, users_list, save_col_names=False, columns=None):
    if (save_col_names):
        np.savetxt(f, [columns], delimiter=';', fmt='%s')
    np.savetxt(f, [user for user in users_list], delimiter=';', fmt='%s')
