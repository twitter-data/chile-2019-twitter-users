#!/usr/bin/env python

import twitter_wrapper as tw
import file_handler as fh
import sys
import os

def get_users(hashtag, num_of_tweets, user_ids):
    api = tw.get_api("api_keys/consumer.json", "api_keys/access.json")
    return tw.get_users_from_search(api, hashtag, user_ids, num_of_tweets)

if __name__ == "__main__":
    hashtag = 'Rechazo'
    num_of_tweets = 1000
    filename = 'data/users.csv'

    if len(sys.argv) >= 2:
        hashtag = sys.argv[1]

    if (len(sys.argv) >= 3):
        num_of_tweets = int(sys.argv[2])

    if (len(sys.argv) >= 4):
        filename = sys.argv[3]

    if os.path.exists(filename):
        f = open(filename, 'r')
        user_ids = fh.read_user_ids(f)
        f.close()
    else:
        user_ids = []
    
    users_list, columns = get_users(hashtag, num_of_tweets, user_ids)

    if (os.path.exists(filename)):
        f = open(filename, 'a')
        fh.save_users(f, users_list, save_col_names=False)
    else:
        f = open(filename, 'w')
        fh.save_users(f, users_list, save_col_names=True, columns=columns)
    f.close()
