#!/usr/bin/env python

import sys
import file_handler as fh
import twitter_wrapper as tw

def merge_users(users1, users2):
    merged_users = {}
    return merged_users

def merge_files(fn1, fn2, out):
    fi1 = open(fn1, 'r')
    users1 = fh.read_users(fi1)
    fi1.close()

    fi2 = open(fn2, 'r')
    users2 = fh.read_users(fi2)
    fi2.close()

    users3 = merge_users(users1, users2)
    f = open(out, 'w')
    fh.save_users(f, users3, save_col_names=True, tw.get_column_names())
    f.close()

def main():
    filename1 = sys.argv[1]
    filename2 = sys.argv[2]

    if len(sys.argv) >= 4:
        output = sys.argv[3]
    else:
        output = filename1

    merge_files(filename1, filename2, output)

if __name__ == "__main__":
    main()
