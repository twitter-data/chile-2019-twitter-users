#!/usr/bin/sh

# Apruebo
exec ./src/main.py Apruebo 1000 data/usuarios_apruebo.csv
exec ./src/main.py YoApruebo 1000 data/usuarios_apruebo.csv
exec ./src/main.py YoVotoApruebo 1000 data/usuarios_apruebo.csv

# Rechazo
exec ./src/main.py Rechazo 1000 data/usuarios_rechazo.csv
exec ./src/main.py YoRechazo 1000 data/usuarios_rechazo.csv
exec ./src/main.py YoVotoRechazo 1000 data/usuarios_rechazo.csv
exec ./src/main.py RechazoCrece 1000 data/usuarios_rechazo.csv
